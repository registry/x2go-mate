FROM registry.gitlab.com/registry/x2go-base:master

RUN apt-get update \
    && apt-get install -y tasksel x2gomatebindings \
    && apt update \
    && tasksel install ubuntu-mate-desktop \
    && rm -f \
        /etc/xdg/autostart/blueman.desktop \
        /etc/xdg/autostart/mate-power-manager.desktop \
        /etc/xdg/autostart/mate-screensaver.desktop \
        /etc/xdg/autostart/orca-autostart.desktop
